# joshbot

Parse email and look for certain anomalies

```
cp env.sh.example env.sh
```

Edit env.sh to your values, and then before invoking the python set your
env:

```
source env.sh
```

https://docs.python.org/3/library/email.examples.html
