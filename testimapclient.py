#!/usr/bin/env python3

import os
import re
import email
import json
from imapclient import IMAPClient
import getpass, imaplib
import pprint
pp = pprint.PrettyPrinter(indent=4)

# https://imapclient.readthedocs.io/en/2.1.0/concepts.html

# Get environment variables
JOSHBOT_SMTP_HOST = os.getenv('JOSHBOT_SMTP_HOST')
JOSHBOT_SMTP_PORT = os.getenv('JOSHBOT_SMTP_PORT')
JOSHBOT_SMTP_USER = os.getenv('JOSHBOT_SMTP_USER')
JOSHBOT_SMTP_PASSWORD = os.environ.get('JOSHBOT_SMTP_PASSWORD')

# context manager ensures the session is cleaned up
with IMAPClient(host=JOSHBOT_SMTP_HOST) as client:
    client.login(JOSHBOT_SMTP_USER, JOSHBOT_SMTP_PASSWORD)
    client.select_folder('INBOX')

    # search criteria are passed in a straightforward way
    # (nesting is supported)
    messages = client.search(['NOT', 'DELETED'])
    #pp.pprint(messages)

    # fetch selectors are passed as a simple list of strings.
    response = client.fetch(messages, ['FLAGS', 'RFC822.SIZE'])
    #pp.pprint(response)

    # `response` is keyed by message id and contains parsed,
    # converted response items.
    # for message_id, data in response.items():
        # print('{id}: {size} bytes, flags={flags}'.format(
            # id=message_id,
            # size=data[b'RFC822.SIZE'],
            # flags=data[b'FLAGS']))


    for uid, message_data in client.fetch(messages, 'RFC822').items():
        email_message = email.message_from_bytes(message_data[b'RFC822'])
        #print(uid, email_message.get('From'), email_message.get('Subject'), email_message.get_body(preferencelist='plain'))
        # print(uid, email_message.get('From'), email_message.get('Subject'))
        # pp.pprint(email_message.__dict__)
        # pp.pprint(vars(email_message))
        # pp.pprint(dir(email_message))
        # pp.pprint(email_message.get_payload())
        # pp.pprint(email_message.as_string())
        #pp.pprint(json.dumps(email_message.items()))
        #print(json.dumps(email_message.items()))
        pp.pprint(re.search("(\S+)snowagenda", email_message.as_string()))
