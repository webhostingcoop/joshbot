#!/usr/bin/env python3
# https://github.com/ikvk/imap_tools

import os
from imap_tools import MailBox, Q
import getpass, imaplib
import pprint
pp = pprint.PrettyPrinter(indent=4)

# Get environment variables
JOSHBOT_SMTP_HOST = os.getenv('JOSHBOT_SMTP_HOST')
JOSHBOT_SMTP_PORT = os.getenv('JOSHBOT_SMTP_PORT')
JOSHBOT_SMTP_USER = os.getenv('JOSHBOT_SMTP_USER')
JOSHBOT_SMTP_PASSWORD = os.environ.get('JOSHBOT_SMTP_PASSWORD')

# get list of email subjects from INBOX folder
with MailBox(JOSHBOT_SMTP_HOST).login(JOSHBOT_SMTP_USER, JOSHBOT_SMTP_PASSWORD) as mailbox:
    mailbox.folder.set('INBOX')
    subjects = [msg.subject for msg in mailbox.fetch()]
    pp.pprint(subjects)
