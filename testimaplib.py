#!/usr/bin/env python3
# https://stackoverflow.com/questions/2230037/how-to-fetch-an-email-body-using-imaplib-in-python

import os
import getpass, imaplib
import pprint
pp = pprint.PrettyPrinter(indent=4)

# Get environment variables
JOSHBOT_SMTP_HOST = os.getenv('JOSHBOT_SMTP_HOST')
JOSHBOT_SMTP_PORT = os.getenv('JOSHBOT_SMTP_PORT')
JOSHBOT_SMTP_USER = os.getenv('JOSHBOT_SMTP_USER')
JOSHBOT_SMTP_PASSWORD = os.environ.get('JOSHBOT_SMTP_PASSWORD')

M = imaplib.IMAP4_SSL(host=JOSHBOT_SMTP_HOST,port=JOSHBOT_SMTP_PORT)
# M.login(getpass.getuser(), getpass.getpass())
M.login(JOSHBOT_SMTP_USER, JOSHBOT_SMTP_PASSWORD)
M.select()
typ, data = M.search(None, 'ALL')
#pp.pprint(data[0].split())
pp.pprint(data)
for num in data[0].split():
    typ, data = M.fetch(num, '(RFC822)')
    #print('Message %s\n%s\n' % (num, data[0][1]))
    print('Gillette commercial')
    pp.pprint(data)
M.close()
M.logout()
